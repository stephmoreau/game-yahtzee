console.log('script.js has loaded');
// VARIABLES
let numGames = 3;
let numPlayers = 1;
let numDice = 5;
let numSides = 6;
let maxRolls = 3;

let stopRollTimer = new Array();
let intervalTimer = new Array();
let dicePips = new Array();
let curRoll = 0;
let curGame = 1;

let playerScore;

/**
 * Initialize the game
 */
function init(){
	console.log("1111");
  setupScorecard();
  // resetGame();
}


/**
 * Rolling the dice action
 */
function rollDice(){
  disable('btnRoll');
  if (curRoll < maxRolls){
    curRoll++;
    
    if (curRoll >= maxRolls) hide('btnRoll');

    clearPossibilities();
    
    for ( let d = 1; d <= numDice; d++ ){
      let curDice = document.getElementById('dice_' + d);
      curDice.disabled = "disabled";
      if ( !document.getElementById('dice_' + d).classList.contains("locked")){
        intervalTimer[d] = setInterval(alternateDicePip, 20, "dice_"+d)
      }
    }
    stopRollTimer = setTimeout(stopAllDice, 1500)
  } else {
    
    hide('btnRoll');
  }
}
/**
 * Stop all the diece from rolling
 */
function stopAllDice(){
  dicePips = []; 
  for ( let d = 1; d <= numDice; d++ ){
    let curDice = document.getElementById('dice_' + d);
    curDice.disabled = "";
    clearInterval( intervalTimer[d] );
    dicePips.push( curDice.dataset.pips )
  }
  enable('btnRoll');
  showPossibilities();
}
/**
 * Show the user the possible scored they can make based on their current dices
 */
function showPossibilities(){
  //upper


  console.log(dicePips);
  showPossibleUppser();

}

function showPossibleUppser(){
  for ( let loopIndex = 1; loopIndex <= numSides; loopIndex++ ){
    let pipCount = searchArrayFor(dicePips, loopIndex);
    console.log( pipCount );
    if (pipCount!= 0){
      document.getElementById('U' + loopIndex + '_' + curGame).disabled = "";
      document.getElementById('U' + loopIndex + '_' + curGame).value = (pipCount * loopIndex);
    }
  }
}

/**
 * Clear all values from inputs for the current game what are not saved
 */
function clearPossibilities(){
  //  clear all posibilities from current game that are not locked in
  let allInputs = document.getElementsByClassName('inputScoreGame_' + curGame);
  for (let index = 0; index < allInputs.length; index++){
    if ( !allInputs[index].classList.contains("locked") ){
      allInputs[index].value = "";
    }
  }

}

//after guess was made, setup to start a new round.
function startNewRoll(){
  clearPossibilities();
  //enable roll button;
}

/**
 * Save the current dice so it does no get rolled
 */
function saveDice(diceId){
  console.log(diceId);
  document.getElementById( diceId ).toggleAttribute("readonly");
  document.getElementById( diceId ).classList.toggle("locked");
  document.getElementById( "save_" + diceId ).classList.toggle("saved");
}


/**
 * Search thru an array for a specific item.
 * @param {array} searchArray Array to search.
 * @param {string} searchFor What to look for in the array.
 * @return {number} Number of items found in the array.
 */
function searchArrayFor(searchArray, searchFor){
  let filterList = searchArray.filter(function(item){ return item == searchFor; });
  return parseInt( filterList.length );

}
/**
 * Alternate the dice numbers
 * @param {string} diceId DOM ID of the dice 
 */
function alternateDicePip( diceId ){
  let randomPip = randomInt();
  document.getElementById(diceId).className  = "dice_number_" + randomPip;
  document.getElementById(diceId).dataset.pips = randomPip;
}
/**
 * Random number generator.
 */
function randomInt(maxInt = numSides){
  return Math.floor( (Math.random() * maxInt) + 1 )
}
/**
 * Hide the element
 * @params {string} ID of the element to hide.
 */
function hide(elemId){
  document.getElementById(elemId).style.display = "none";
}
/**
 * Show the element
 * @param {string} elemId  ID of the element to show.
 * @param {string} {playType="block"} Display type to show
 */
function show(elemId, displayType="block"){
  document.getElementById(elemId).style.display = "none";
}
/**
 * Disable the element
 * @params {string} ID of the element to Disable.
 */
function disable(elemId){
  document.getElementById(elemId).disabled = "disabled";
}
/**
 * Show the element
 * @param {string} elemId  ID of the element to enable.
 */
function enable(elemId){
  document.getElementById(elemId).disabled = "";
}
 /**
 * Create the table for the scoring.
 */
function setupScorecard(){
  generateHeaderRow();

  generateTitleRow("Upper Section");
  generateRow("Aces", "Count and add only aces", "U1",);
  generateRow("Twos", "Count and add only twos", "U2");
  generateRow("Threes", "Count and add only threes", "U3");
  generateRow("Four", "Count and add only fours", "U4");
  generateRow("Fives", "Count and add only fives", "U5");
  generateRow("Sixes", "Count and add only sixes", "U6");
  generateFullRow('Total Scores', "UT")
	generateRow("Bonus", "Upper over 65 = 35 pts", "UB");
  generateFullRow('Total of upper section', "UT_U")

  generateTitleRow("Lower Section");
  generateRow("3 of a kinds", "Add total of all dice", "3K",);
  generateRow("4 of a kinds", "Add total of all dice", "4K",);
  generateRow("Full House", "25 pts", "FH");
  generateRow("Small Straight", "30 pts", "SS");
  generateRow("Large Straight", "40 pts", "LS");
  generateRow("Yahtzee", "50 pts", "YZ");
  generateRow("Chance", "Add total of all dice", "CH");

  generateFullRow('Lower section total', "LT")
  generateFullRow('Upper section total', "UT_L")
  generateFullRow('Combined total of upper and lower section', "TS")


}
/**
 * Generate the header row for the table.
 * @param {number} player Player number for the rows.
 */
function generateHeaderRow(player=1){
  let newTBody = document.createElement( 'tbody' );
  let newRow = document.createElement( 'tr' );

  let cellTitle = document.createElement( 'th' );
  cellTitle.innerText = "Player " + player;
  newRow.appendChild( cellTitle );
  
  let cellInfo = document.createElement( 'th' );
  cellInfo.innerText = "How to score";
  newRow.appendChild( cellInfo );
  
  for( let c = 1; c <= numGames; c++ ){
    let cellData = document.createElement( 'th' );
    cellData.innerText = "Game " + c;
    cellData.className = "short";
    newRow.appendChild( cellData );
  }

  newTBody.appendChild( newRow );
  document.getElementById('scorecard').appendChild( newTBody );
}
/**
 * Generate a full row for the table.
 * @param {string} title Title for the row.
 */
function generateTitleRow(title) {
  let newRow = document.createElement( 'tr' );

  let cellTitle = document.createElement( 'th' );
  cellTitle.innerText = title;
  cellTitle.colSpan = 2 + (numGames * numPlayers);
  newRow.appendChild( cellTitle );
      
  document.getElementById('scorecard').appendChild( newRow );
}
/**
 * Generate a row for the table.
 * @param {string} title Title for the row.
 * @param {string} scoring Instructions on how to score.
 * @param {string} elemId ID to be used in the input for the row.
 */
function generateRow(title, scoring, elemId) {
  let newRow = document.createElement( 'tr' );

  let cellTitle = document.createElement( 'th' );
  cellTitle.innerText = title;
  newRow.appendChild( cellTitle );
  
  let cellInfo = document.createElement( 'td' );
  cellInfo.className = "instructions";
  cellInfo.innerText = scoring;
  newRow.appendChild( cellInfo );
  
  generateScoringCell( elemId, newRow );
    
  document.getElementById('scorecard').appendChild( newRow );
}
/**
 * Generate a full row for the table.
 * @param {string} title Title for the row.
 * @param {string} elemId ID to be used in the input for the row.
 */
function generateFullRow(title, elemId) {
  let newRow = document.createElement( 'tr' );

  let cellTitle = document.createElement( 'th' );
  cellTitle.innerText = title;
  cellTitle.colSpan = 2;
  newRow.appendChild( cellTitle );
    
  generateScoringCell( elemId, newRow, true );
  // for( let c = 0; c < numGames; c++ ){
  //   let cellData = document.createElement( 'td' );
  //   if (elemId !== null){
  //     let cellInput = document.createElement( 'input' );
  //     cellInput.type = "button";
  //     cellInput.className = "score upperScore";
  //     cellInput.id = elemId + "_" + c;
  //     cellInput.dataset.score = elemId;
  //     cellData.appendChild( cellInput );
  //   }
  //   newRow.appendChild( cellData );
  // }
    
  document.getElementById('scorecard').appendChild( newRow );
}
/**
 * Generate the scoring input button for the scorecard.
 * @param {string}  elemId Element id for the input.
 * @param {object}  newRow ROW element to add the cells to.
 * @param {boolean} [cellLocked=false]  Indicate if a cell should be clickable by user.
 */
function generateScoringCell( elemId, newRow, cellLocked = false ){
  for( let c = 1; c <= numGames; c++ ){
    let cellData = document.createElement( 'td' );
    if (elemId !== null){
      let cellInput = document.createElement( 'input' );
      cellInput.type = "button";
      cellInput.value = randomInt(511);
      cellInput.id = elemId + "_" + c;
      cellInput.dataset.score = elemId;
      cellInput.disabled = "disabled";

      if ( cellLocked )
        cellInput.className = "lockedScore_" + c + " score game_"+c;
      else
        cellInput.className = "inputScoreGame_" + c + " score game_"+c;

      cellData.appendChild( cellInput );

    }
    newRow.appendChild( cellData );
  }
  return newRow;
}

// EVENT LISTENERS
window.addEventListener('load', init());
