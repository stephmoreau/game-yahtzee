## Yahtzee Style Game ##
*Only Html and CSS were used to create this game.*
Inspired by student project. I attempt to make my own version of a games they have submitted using only HTML and CSS.
 
---

## Extra files used
List of files besides out index.html and style.css
- Google Fonts to make it look a little nicer
- [Revue Font](https://www.wfonts.com/font/revue) - for Logo


---

## Brainstrom
- Include possibility of multiple players

---

## Future Tasks ##
Notes on possible options/functionality that could be added in HTML or CSS

1. Nothing yet
